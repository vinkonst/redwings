# React Boilerplate

Production-ready boilerplate for building *universal* web apps with [React](https://github.com/facebook/react) and [Redux](https://github.com/reactjs/redux)

## tl;dr

```
$ git clone https://vinkonst@bitbucket.org/vinkonst/redwings.git
$ cd redwings
$ rm -r .git
$ npm install
$ npm install knex -g
$ knex migrate:latest
$ knex seed:run
$ npm run start
```

## Commands
## NPM Script Commands
 All of the scripts are listed as following:

`npm run <script>`|Description
------------------|-----------
`start`|Run your app on the development server at `localhost:6060`. HMR will be enabled.
`start:production`|Builds the app ready for release and run it on the production server at `localhost:6060`.
`start:prod`|Run your app on the production server only at `localhost:6060`.
`build`|Builds the app ready for release.

## Environment Variables
> NOTE: Any of the environment variables can be made available to the client by explicitly declaring them in the root [Html](src/components/html/Html.js) component. This extra step is required to prevent accidentally leaking sensitive data to the client.

## License
MIT
