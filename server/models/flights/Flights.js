'use strict'

const { Model } = require('../../../orm')

class Flights extends Model {
  static get tableName() {
    return 'flights'
  }

  static get idColumn() {
    return 'id'
  }
}

Flights.getAll = function(params) {
  return this.query()
    .where('departure_date', '>=', params.startDate)
    .where('departure_date', '<=', params.endDate)
    .orderBy('departure_date', 'asc')
}

module.exports = Flights
