'use strict'

const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const debug = require('debug')('app')
const colors = require('colors/safe')
const api = require('./routes/api')
const { knex } = require('../orm')
const knexLogger = require('./middleware/knex-logger')
const app = express()

app.set('port', process.env.PORT)
app.use(compression())
app.use(bodyParser.json())
app.use(fileUpload())

app.use(knexLogger(knex))

if (process.env.WEBPACK_DEV_SERVER === 'true') {
  app.use('/api', (req, res, next) => {
    require('./routes/api')(req, res, next)
  })
  app.use(require('./routes/webpack'))
} else {
  app.use('/api', api)
  app.use(require('./routes/static'))
}

if (process.env.NODE_ENV === 'development') {
  const errorhandler = require('errorhandler')

  errorhandler.title = '¯\\_(ツ)_/¯'
  app.use(errorhandler())
}

app.listen(app.get('port'), () => {
  debug(colors.white(`Server started: http://localhost:${app.get('port')}`))
  debug(colors.grey("Press 'ctrl + c' to terminate server"))
})
