'use strict'

const express = require('express')

const flightsRoutes = require('./flights/flights.routes')

const router = express.Router()

router.use('/flights', flightsRoutes)

module.exports = router
