'use strict'

const express = require('express')
const flightsCtrl = require('./flights.controller')

const router = express.Router()

router.route('/')
  .get(flightsCtrl.getFlights)

module.exports = router
