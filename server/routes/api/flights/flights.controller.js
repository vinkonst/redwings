'use strict'

const Flights = require('../../../models/flights/Flights')

const getFlights = async(req, res) => {
  try {
    const flights = await Flights.getAll({
      startDate: req.query.start_date,
      endDate: req.query.end_date
    })

    res.json({ status: 'success', data: { flights } })
  } catch (error) {
    console.log(error)
    res.status(500).json({ status: 'error', message: 'Error' })
  }
}

module.exports = {
  getFlights
}
