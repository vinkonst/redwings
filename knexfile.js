'use strict'

const path = require('path')

module.exports = {
  development: {
    client: 'pg',
    connection: 'postgres://postgres:@localhost/ak_prices',
    migrations: {
      directory: path.join(__dirname, 'server/db/migrations')
    },
    seeds: {
      directory: path.join(__dirname, 'server/db/seeds')
    },
    shemaName: 'public',
    caseSensitive: false
  },

  test: {
    client: 'pg',
    connection: 'postgres://postgres:@localhost/ak_prices',
    migrations: {
      directory: path.join(__dirname, 'server/db/migrations')
    },
    seeds: {
      directory: path.join(__dirname, 'server/db/seeds')
    },
    shemaName: 'public',
    caseSensitive: false
  },

  production: {
    client: 'pg',
    connection: 'postgres://postgres:@localhost/ak_prices',
    migrations: {
      directory: path.join(__dirname, 'server/db/migrations')
    },
    seeds: {
      directory: path.join(__dirname, 'server/db/seeds')
    },
    shemaName: 'public',
    caseSensitive: false
  }
}
