import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import flights from 'pages/index/reducers/flights/flights'
import filters from 'pages/index/reducers/flights/filters'

const rootReducer = combineReducers({
  flights,
  filters,
  routing: routerReducer
})

export default rootReducer
