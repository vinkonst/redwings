import _ from 'lodash'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'

import Flights from 'pages/index/components/flights/Flights'

const getFlights = state => state.flights.flights

const assignFlightData = (prefix, flight) => ({
  [`${prefix}_carrier`]: flight ? flight.carrier : '-',
  [`${prefix}_flight_number`]: flight ? flight.flight_number : '-',
  [`${prefix}_segments_count`]: flight ? flight.segments_count : '-',
  [`${prefix}_amount`]: flight ? flight.amount : '-',
  [`${prefix}_fare`]: flight ? flight.fare : '-',
  [`${prefix}_taxes`]: flight ? flight.taxes : '-',
  [`${prefix}_fee`]: flight ? flight.fee : '-',
  [`${prefix}_route`]: flight ? `${flight.from_flight}-${flight.to_flight}` : '-',
  [`${prefix}_departure_date`]: flight ? flight.departure_date : '-',
  [`${prefix}_departure_time`]: flight ? flight.departure_time : '-',
  [`${prefix}_arrival_date`]: flight ? flight.arrival_date : '-',
  [`${prefix}_arrival_time`]: flight ? flight.arrival_time : '-',
  [`${prefix}_is_baggage`]: flight ? flight.is_baggage : '-',
  [`${prefix}_is_refund`]: flight ? flight.is_refund : '-'
})

const getGroupedFlights = createSelector(
  [getFlights],
  flights => {
    const groupedByDate = _.groupBy(flights, f => f.departure_date)
    const data = []

    _.each(groupedByDate, (group, key) => {
      const groupedByRoute = _.groupBy(group, f => `${f.from}${f.to}`)
      const result = []

      _.each(groupedByRoute, flights => {
        const rwFlights = _.sortBy(_.filter(flights, f => f.carrier === 'WZ'), data => data.amount)
        const naFlights = _.sortBy(_.filter(flights, f => f.carrier === '5N'), data => data.amount)
        const otherFlights = _.sortBy(_.filter(flights, f => f.carrier !== 'WZ' && f.carrier !== '5N'), data => data.amount)

        const groups = _(otherFlights).chain()
          .groupBy('carrier')
          .sortBy(data => _.first(data).amount)
          .value()

        result.push(
          _.assign({
            id: flights[0].id,
            from: flights[0].from,
            to: flights[0].to
          },
          assignFlightData('rw', rwFlights[0]),
          assignFlightData('na', naFlights[0]),
          assignFlightData('top1', groups[0] ? groups[0][0] : null),
          assignFlightData('top2', groups[1] ? groups[1][0] : null),
          assignFlightData('top3', groups[2] ? groups[2][0] : null)
          )
        )
      })

      data.push({
        date: key,
        result
      })
    })

    return data
  }
)

const mapStateToProps = state => ({
  flights: getGroupedFlights(state)
})

export default connect(mapStateToProps)(Flights)
