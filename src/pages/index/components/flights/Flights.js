import _ from 'lodash'
import moment from 'moment'
import numberFormat from 'number-formatter'
import React from 'react'
import PropTypes from 'prop-types'

// UI
import { Table } from 'antd'

// Styles
import styles from 'pages/index/components/flights/Flights.css'

const createColumn = (prefix, isOther) => {
  const children = []

  if (isOther) {
    children.push({
      className: styles[prefix],
      width: 50,
      title: 'Код',
      dataIndex: `${prefix}_carrier`,
      key: `${prefix}_carrier`
    })
  }

  return {
    title: prefix.toUpperCase(),
    children: _.concat(children, [{
      className: styles[prefix],
      width: 160,
      title: 'Маршрут',
      dataIndex: `${prefix}_route`,
      key: `${prefix}_route`,
      render: (text, record) => record[`${prefix}_amount`] > 0 ? (
        <div>
          <div>{record[`${prefix}_route`]}</div>
          <div>Вылет: {moment(record[`${prefix}_departure_date`]).format('DD.MM.YY')} {moment(record[`${prefix}_departure_time`], 'hh:mm:ss').format('hh:mm')}</div>
          <div>Прилет: {moment(record[`${prefix}_arrival_date`]).format('DD.MM.YY')} {moment(record[`${prefix}_arrival_time`], 'hh:mm:ss').format('hh:mm')}</div>
          <div>Сегментов: {record[`${prefix}_segments_count`]}</div>
        </div>
      ) : '-'
    }, {
      className: styles[prefix],
      width: 50,
      title: 'Рейс',
      dataIndex: `${prefix}_flight_number`,
      key: `${prefix}_flight_number`
    }, {
      className: styles[prefix],
      title: 'Стоимость',
      width: 120,
      dataIndex: `${prefix}_amount`,
      key: `${prefix}_amount`,
      render: (text, record) => record[`${prefix}_amount`] > 0 ? (
        <div className={!isOther ? record[`${prefix}_amount`] > record.top1_amount ? styles.greater : styles.lower : ''}>
          <div className={styles.amount}>
            {numberFormat('# ###.', record[`${prefix}_amount`])}
            {' '}
            <span className={styles.amount_perc}>
              { !isOther ?
                record[`${prefix}_amount`] > record.top1_amount ?
                  numberFormat('###0.00 %', record[`${prefix}_amount`] / record.top1_amount * 100 - 100) :
                  numberFormat('###0.00 %', record.top1_amount / record[`${prefix}_amount`] * 100 - 100) : '' }
            </span>
          </div>
          <div>Тариф: {numberFormat('# ##0.', record[`${prefix}_fare`])}</div>
          <div>Таксы: {numberFormat('# ##0.', record[`${prefix}_taxes`])}</div>
          <div>Сборы: {record[`${prefix}_fee`]}</div>
        </div>
      ) : '-'
    }, {
      className: styles[prefix],
      title: 'Условия',
      width: 80,
      dataIndex: `${prefix}_conditions`,
      key: `${prefix}_conditions`,
      render: (text, record) => record[`${prefix}_amount`] > 0 ? (
        <div>
          <div style={{ textDecoration: record[`${prefix}_is_baggage`] === false ? 'line-through' : 'none' }}>Багаж</div>
          <div style={{ textDecoration: record[`${prefix}_is_refund`] === false ? 'line-through' : 'none' }}>Обмен</div>
        </div>
      ) : '-'
    }])
  }
}

const columns = [{
  title: 'Откуда',
  dataIndex: 'from',
  key: 'from',
  width: 75,
  fixed: 'left'
}, {
  title: 'Куда',
  dataIndex: 'to',
  key: 'to',
  width: 75,
  fixed: 'left'
},
createColumn('rw', false),
createColumn('na', false),
createColumn('top1', true),
createColumn('top2', true),
createColumn('top3', true)
]

const Flights = ({ flights }) => {
  console.log(flights)

  return (
    <div className={styles.root}>
      {_.map(flights, item => (
        <Table
          title={() => <div className={styles.title}>{moment(item.date).format('DD.MM.YYYY')}</div>}
          key={item.date}
          rowKey="id"
          dataSource={item.result}
          columns={columns}
          scroll={{ x: 2300, y: 650 }}
          bordered
          pagination={false}
        />
      ))}
    </div>
  )
}

Flights.propTypes = {
  flights: PropTypes.array.isRequired
}

export default Flights
