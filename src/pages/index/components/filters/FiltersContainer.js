import { connect } from 'react-redux'

import Filters from 'pages/index/components/filters/Filters'

const getFilters = state => state.filters

const mapStateToProps = state => ({
  filters: getFilters(state)
})

export default connect(mapStateToProps)(Filters)
