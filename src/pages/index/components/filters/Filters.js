import moment from 'moment'
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

// Actions
import { changeFilter } from 'pages/index/actions/flights/filters'
import { fetchFlights } from 'pages/index/actions/flights/flights'

// UI
import { Button, Form, DatePicker } from 'antd'

// Styles
import styles from 'pages/index/components/filters/Filters.css'

class Filters extends PureComponent {
  handleSubmit = ev => {
    ev.preventDefault()

    this.props.dispatch(fetchFlights())
  }

  render() {
    const { filters, dispatch } = this.props

    return (
      <Form layout="inline" onSubmit={this.handleSubmit} className={styles.root}>
        <Form.Item>
          <DatePicker.RangePicker
            value={[moment(filters.date[0]), moment(filters.date[1])]}
            format="DD.MM.YYYY"
            onChange={date => dispatch(changeFilter({
              name: 'date',
              value: date
            }))}
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
          >
            Обновить
          </Button>
        </Form.Item>
      </Form>
    )
  }
}

Filters.propTypes = {
  filters: PropTypes.object.isRequired
}

export default Filters
