import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Actions
import { fetchFlightsIfNeeded } from 'pages/index/actions/flights/flights'

// UI
import { Spin } from 'antd'

// Components
import Error from 'components/lib/error/Error'

import Filters from 'pages/index/components/filters/FiltersContainer'
import Flights from 'pages/index/components/flights/FlightsContainer'

// Styles
import styles from 'pages/index/Index.css'

export class Index extends Component {
  componentDidMount() {
    const { dispatch } = this.props

    Index.fetchData({ dispatch })
  }

  render() {
    const { isFetching, error } = this.props

    return (
      <div className={styles.root}>
        <Filters />
        { error && <Error>{error}</Error> }
        { !error && (
          <Spin spinning={isFetching}>
            <Flights />
          </Spin>
        ) }
      </div>
    )
  }
}

Index.fetchData = function({ dispatch }) {
  return dispatch(fetchFlightsIfNeeded())
}

Index.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  flights: PropTypes.array.isRequired
}

export default Index
