import { createAction } from 'redux-act'

export const changeFilter = createAction(
  'change filter'
)
