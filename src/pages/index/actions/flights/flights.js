import { createAction } from 'redux-act'
import api from 'api'

export const fetchFlightsRequest = createAction(
  'fetch flights request'
)

export const fetchFlightsSuccess = createAction(
  'fetch flights success',
  data => data
)

export const fetchFlightsFailure = createAction(
  'fetch flights failure',
  error => error.message
)

export function fetchFlights() {
  return (dispatch, getState) => {
    dispatch(fetchFlightsRequest())

    const { filters } = getState()

    return api.flightsApi.getFlights(filters)
      .then(data => dispatch(fetchFlightsSuccess(data)))
      .catch(error => dispatch(fetchFlightsFailure(error)))
  }
}

function shouldFetchFlights(state) {
  const { flights, isFetching } = state.flights

  return !isFetching && !flights.length
}

export function fetchFlightsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchFlights(getState())) {
      return dispatch(fetchFlights())
    }
  }
}
