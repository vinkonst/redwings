import { createReducer } from 'redux-act'

import {
  fetchFlightsRequest,
  fetchFlightsSuccess,
  fetchFlightsFailure
} from 'pages/index/actions/flights/flights'

const DEFAULT_STATE = {
  isFetching: false,
  error: false,
  flights: []
}

const flightsReducer = createReducer({
  [fetchFlightsRequest]: state => ({
    ...state,
    isFetching: true,
    error: false
  }),

  [fetchFlightsSuccess]: (state, payload) => ({
    ...state,
    isFetching: false,
    flights: payload.flights
  }),

  [fetchFlightsFailure]: (state, payload) => ({
    ...state,
    isFetching: false,
    error: payload
  })
}, DEFAULT_STATE)

export default flightsReducer
