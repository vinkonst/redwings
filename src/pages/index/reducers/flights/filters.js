import moment from 'moment'
import { createReducer } from 'redux-act'

import {
  changeFilter
} from 'pages/index/actions/flights/filters'

const DEFAULT_STATE = {
  date: [moment().add(1, 'day'), moment().add(1, 'day')]
}

const filtersReducer = createReducer({
  [changeFilter]: (state, payload) => ({
    ...state,
    [payload.name]: payload.value
  })
}, DEFAULT_STATE)

export default filtersReducer
