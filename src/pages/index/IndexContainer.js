import { connect } from 'react-redux'

import Index from 'pages/index/Index'

const flightsSelector = state => state.flights

const mapStateToProps = state => ({
  ...flightsSelector(state)
})

export default connect(mapStateToProps)(Index)
