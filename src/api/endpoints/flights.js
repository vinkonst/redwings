import moment from 'moment'

export default class OrdersApi {
  constructor(apiCore) {
    this.API = apiCore
    this.path = `${process.env.API_ENDPOINT}/flights`
  }

  getFlights(filters) {
    return this.API.get(this.path, {
      start_date: moment(filters.date[0]).format('YYYY-MM-DD'),
      end_date: moment(filters.date[1]).format('YYYY-MM-DD')
    })
  }
}
