import ApiCore from './ApiCore'

// Apis endpoints
import * as endPoints from './endpoints'

class API {
  constructor() {
    this.apiCore = new ApiCore()

    for (const endPoint in endPoints) {
      this[endPoint] = new endPoints[endPoint](this.apiCore)
    }
  }
}

const api = new API()

export default api
