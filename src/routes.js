import React from 'react'
import { Route, IndexRoute } from 'react-router'

import App from 'containers/app/App'
import NotFound from 'pages/notfound/NotFound'

// Pages
import IndexLoadable from 'pages/index/IndexLoadable'

const getRoutes = () => (
  <Route path="/" component={App}>
    <IndexRoute component={IndexLoadable} />
    <Route path="*" component={NotFound} />
  </Route>
)

export default getRoutes

export { NotFound as NotFoundComponent }
