import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'

// Styles
import styles from 'containers/app/App.css'

const DEFAULT_TITLE = 'Red Wings Test Task'

function App({ children }) {
  return (
    <div className={styles.root}>
      <Helmet
        titleTemplate={`%s | ${DEFAULT_TITLE}`}
        defaultTitle={DEFAULT_TITLE}
      />

      { children }
    </div>
  )
}

App.propTypes = {
  children: PropTypes.node
}

function mapStateToProps(state) {
  return { ...state.locale }
}

export default connect(mapStateToProps)(App)
